MAXPUERTOS = 0xFF

MEMMAX = 0xFFFF
PORTMAX = 255

ROM_MIN = 0x0000
ROM_MAX = 0x1FFF

VIDEO_MIN = 0x2400
VIDEO_MAX = 0x4000

"""
CONSTANTES PARA LAS TECLAS
"""
COIN_INSERTED = 0x01
TWO_PLAYERS   = 0x02
ONE_PLAYER    = 0x04
P1_FIRE       = 0x10
P1_LEFT       = 0x20
P1_RIGHT      = 0x30

#fps = 60
#INSTRUCCIONES_POR_FRAME = 2000000 / (2*fps) / 7
INSTRUCCIONES_POR_FRAME = 240



"""
	Valores del Puerto 1 
    0   Coin slot (1=coin inserted)
    1   Two players button
    2   One player button
    3   n/a
    4   Player one - Fire button
    5   Player one - Left button
    6   Player one - Right button
    7   n/a
"""

MONEDA_INSERTADA = 1
DOS_JUGADORES    = 2
UN_JUGADOR	 = 4
DISPARO_JUGADOR1 = 16
IZQ_JUGADOR1     = 32
DER_JUGADOR1     = 64

"""
    Valores del Puerto 2
    0,1 DIP switch: number of ships
    2	DIP switch: easy/hard mode (0=hard, 1=easy)
    3	n/a
    4	Player two - Fire button
    5	Player two - Left button
    6	Player two - Right button
    7	n/a
"""

NAVES_DIP_1	 =  00
NAVES_DIP_2	 =  01
NAVES_DIP_3	 =  02
NAVES_DIP_4	 =  03
JUEGO_HARD	 =  00
JUEGO_EASY       =  01
DISPARO_JUGADOR2 = 16
IZQ_JUGADOR2     = 32
DER_JUGADOR2     = 64

"""
    Valores de escritura del Puerto 3
    0   Spaceship (looped) sound
    1   Shot sound
    2   Base (your ship) hit sound
    3   Invader hit sound
"""

SONIDO_NAVE	= 1
SONIDO_DISPARO  = 2
SONIDO_BASE     = 4
SONIDO_INVADER  = 8

"""
NOTA: Parece que esta medida en ciclos
de reloj, y no en ciclos de maquina.
"""
"""
pixmap 	 : PGdkPixmap  (* Backing pixmap for drawing area *)
  window,
  drawing_area,
  vbox           : PGtkWidget
  button         : PGtkWidget
  i		 : word
  IntAB		 : boolean
"""

