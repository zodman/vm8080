from .alu import ALU
from .alu16 import ALU16
from .psw import PSW
from .enums import MEMMAX
from .instrucciones import *

class Procesador(object):
    def __init__(self, m, p):
        self.ALU = ALU(self)
        self.ALU16 = ALU16(self)
        self.PSW = PSW()
        self.Mem = m		#Asigna un enlace a la memoria creada
        self.Puertos = p	#Asigna un enlace a los puertos creados
        self.Reset()

    def Reset(self):
        self.A = 0x00
        self.B = 0x00
        self.C = 0x00
        self.D = 0x00
        self.E = 0x00
        self.H = 0x00
        self.L = 0x00
        self.PC = 0x0000
        self.SP = MEMMAX
        self.I = 0x00
        self.mHALT = False
        self.PSW.Reset()
        self.mHALT = False
        self.InterrupcionesActivas = True
        self.CiclosEjecutados = 0
    #	if ObservadorReset:
    #		ObservadorReset

    def Interrupcion(self,Addr):
        self.Desplegar()
        self.InterrupcionesActivas = False
        i = HW_RST(self)
        i.Ejecutar(Addr)

    def Desplegar(self):
        if self.PSW.C:
            s = 'c'
        else:
            s = '-'
        if self.PSW.P:
            s = s + 'p'
        else:
            s = s + '-'
        if self.PSW.S:
            s = s + 's'
        else:
            s = s + '-'
        if self.PSW.Ac:
            s = s + 'a'
        else:
            s = s + '-'
        if self.PSW.Z:
            s = s + 'z'
        else:
            s = s + '-'
        s = s +' A= ' + hex(self.A)
        s = s +' B= ' + hex(self.B)
        s = s +' C= ' + hex(self.C)
        s = s +' D= ' + hex(self.D)
        s = s +' E= ' + hex(self.E)
        s = s +' H= ' + hex(self.H)
        s = s +' L= ' + hex(self.L)
        s = s +' SP= ' + hex(self.SP)
        s = s +' PC= ' + hex(self.PC)
#        #print s

    def ObtenerDireccionHL(self):
        Valor = (self.H << 8) | self.L
#        #print '[' + hex(Valor), '],'
        return Valor

    def DecodificarPort(self):
        Port = self.Mem.Leer(self.PC+1)
        #print hex(Port)
        return Port

    def DecodificarRegisterPair(self):
        DDDMASK = 0x30
        r = (self.I & DDDMASK) >> 4
        #if r == 0:
            ##print 'BC'
        #elif r == 1:
            ##print 'DE'
        #elif r == 2:
            ##print 'HL'
        #elif r == 3:
            ##print 'SP'
        #else:
            ##print 'Error decodificando Register Pair'
        return r

    def ObtenerValorRegisterPair(self):
        r = self.DecodificarRegisterPair()
        if r == 0:
            Valor16 = (self.B << 8) | self.C
        elif r == 1:
            Valor16 = (self.D << 8) | self.E
        elif r == 2:
            Valor16 = (self.H << 8) | self.L
        elif r == 3:
            Valor16 = self.SP
        return Valor16

    def DecodificarDireccionRegisterPair(self):
        DDDMASK = 0x10
        r = (self.I & DDDMASK) >> 4
       # if r == 0:
            ##print 'BC'
        #elif r == 1:
            ##print 'DE'
        #else:
            ##print 'Error decodificando Direccion Register Pair'
        return r

    def ObtenerDireccionRegisterPair(self):
        r = self.DecodificarDireccionRegisterPair()
        if r == 0:
            Addr = (self.B << 8) | self.C
        elif r == 1:
            Addr = (self.D << 8) | self.E
        return Addr

    def DecodificarRegisterPairPila(self):
        DDDMASK = 0x30
        r = (self.I & DDDMASK) >> 4
       # if r == 0:
            ##print 'BC'
        #elif r == 1:
            ##print 'DE'
        #elif r == 2:
            ##print 'HL'
        #elif r == 3:
            ##print 'AF'
        #else:
            ##print 'Error decodificando Register Pair Pila'
        return r

    def ObtenerInmediato(self):
        Inmediato = self.Mem.Leer(self.PC+1)
        #print hex(Inmediato)
        return Inmediato

    def ObtenerDobleInmediato(self):
        pp = self.Mem.Leer(self.PC+2)
        qq = self.Mem.Leer(self.PC+1)
        #print hex(pp << 8 | qq)
        return pp,qq

    def ObtenerDireccionInmediata(self):
        pp = self.Mem.Leer(self.PC+2)
        qq = self.Mem.Leer(self.PC+1)
        DireccionInmediata = (pp << 8) | qq
#        #print '[', hex(DireccionInmediata), ']'
        return DireccionInmediata

    def DecodificarOrigen(self):
        SSSMASK = 0x07
        r= self.I & SSSMASK
       # if r == 0:
            ##print 'B'
        #elif r == 1:
            ##print 'C'
        #elif r == 2:
            ##print 'D'
        #elif r == 3:
            ##print 'E'
        #elif r == 4:
            ##print 'H'
        #elif r == 5:
            ##print 'L'
        if r == 6:
            self.ObtenerDireccionInmediata()
        #elif r == 7:
            ##print 'A'
        #else:
            ##print 'Error decodificando Origen'
        return r

    def ObtenerOperandoOrigen(self):
        sss = self.DecodificarOrigen()
        if sss == 0:
            operando = self.B
        elif sss == 1:
            operando = self.C
        elif sss == 2:
            operando = self.D
        elif sss == 3:
            operando = self.E
        elif sss == 4:
            operando = self.H
        elif sss == 5:
            operando = self.L
        elif sss == 6:
            Addr = self.ObtenerDireccionHL()
            operando = self.Mem.Leer( Addr )
        elif sss == 7:
            operando = self.A
        return operando

    def DecodificarDestino(self):
        DDDMASK = 0x38
        r = (self.I & DDDMASK) >> 3
#        if r == 0:
            ##print 'B,',
        #elif r == 1:
            ##print 'C,',
        #elif r == 2:
            ##print 'D,',
        #elif r == 3:
            ##print 'E,',
        #elif r == 4:
            ##print 'H,',
        #elif r == 5:
            ##print 'L,',
        #elif r == 6:
            ##print '[HL],',
        #elif r == 7:
            ##print 'A,',
        #else:
            ##print 'Error decodificando destino'
        return r

    def CicloVonNeumann(self):
        
        self.I = self.Mem.Leer(self.PC)			#Fetch

        if self.I == 0x00:				#Decodify
            instr = NOP(self)
        elif self.I in [0x06,0x0e,0x16,0x1e,0x26,0x2e,0x36,0x3e]:
            instr = MVI(self)
        elif self.I in [0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,\
               0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,\
               0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,\
               0x70,0x71,0x72,0x73,0x74,0x75,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F]:
            instr = MOV(self)
        elif self.I == 0x76:
            instr = HLT(self)
        elif self.I in [0x80,0x81,0x83,0x84,0x85,0x86,0x87]:
            instr = ADD(self)
        elif self.I in [0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F]:
            instr = ADC(self)
        elif self.I == 0xC6:
            instr = ADI(self)
        elif self.I == 0xCE:
            instr = ACI(self)
        elif self.I in [0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7]:
            instr = ANA(self)
        elif self.I == 0xE6:
            instr = ANI(self)
        elif self.I == 0xCD:
            instr = CALL(self)
        elif self.I == 0xDC:
            instr = CC(self)
        elif self.I == 0xFC:
            instr = CM(self)
        elif self.I == 0x2F:
            instr = CMA(self)
        elif self.I == 0x3F:
            instr = CMC(self)
        elif self.I in [0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF]:
            instr = CMP(self)
        elif self.I == 0xD4:
            instr = CNC(self)
        elif self.I == 0xC4:
            instr = CNZ(self)
        elif self.I == 0xF4:
            instr = CP(self)
        elif self.I == 0xEC:
            instr = CPE(self)
        elif self.I == 0xFE:
            instr = CPI(self)
        elif self.I == 0xE4:
            instr = CPO(self)
        elif self.I == 0xCC:
            instr = CZ(self)
        elif self.I == 0x27:
            instr = DAA(self)
        elif self.I in [0x09,0x19,0x29,0x39]:
            instr = DAD(self)
        elif self.I in [0x05,0x0D,0x15,0x1D,0x25,0x2D,0x35,0x3D]:
            instr = DCR(self)
        elif self.I in [0x0B,0x1B,0x2B,0x3B]:
            instr = DCX(self)
        elif self.I == 0xF3:
            instr = DI(self)
        elif self.I == 0xFB:
            instr = EI(self)
        elif self.I == 0xDB:
            instr = IN(self)
        elif self.I in [0x04,0x0C,0x14,0x1C,0x24,0x2C,0x34,0x3C]:
            instr = INR(self)
        elif self.I in [0x03,0x13,0x23,0x33]:
            instr = INX(self)
        elif self.I == 0xDA:
            instr = JC(self)
        elif self.I == 0xFA:
            instr = JM(self)
        elif self.I == 0xC3:
            instr = JMP(self)
        elif self.I == 0xD2:
            instr = JNC(self)
        elif self.I == 0xC2:
            instr = JNZ(self)
        elif self.I == 0xF2:
            instr = JP(self)
        elif self.I == 0xEA:
            instr = JPE(self)
        elif self.I == 0xE2:
            instr = JPO(self)
        elif self.I == 0xCA:
            instr = JZ(self)
        elif self.I == 0x3A:
            instr = LDA(self)
        elif self.I in [0x1A,0x0A]:
            instr = LDAX(self)
        elif self.I == 0x2A:
            instr = LHLD(self)
        elif self.I in [0x01,0x11,0x21,0x31]:
            instr = LXI(self)
        elif self.I in [0xB0,0xB1,0xB2,0xB3,0xB4,0xB6,0xB7]:
            instr = ORA(self)
        elif self.I == 0xF6:
            instr = ORI(self)
        elif self.I == 0xD3:
            instr = OUT(self)
        elif self.I == 0xE9:
            instr = PCHL(self)
        elif self.I in [0xC1,0xD1,0xE1,0xF1]:
            instr = POP(self)
        elif self.I in [0xC5,0xD5,0xE5,0xF5]:
            instr = PUSH(self)
        elif self.I == 0x17:
            instr = RAL(self)
        elif self.I == 0x1F:
            instr = RAR(self)
        elif self.I == 0xD8:
            instr = RC(self)
        elif self.I == 0xC9:
            instr = RET(self)
        #elif self.I == 0x20:
        #	instr = RIM(self)
        elif self.I == 0x07:
            instr = RLC(self)
        elif self.I == 0xF8:
            instr = RM(self)
        elif self.I == 0xD0:
            instr = RNC(self)
        elif self.I == 0xC0:
            instr = RNZ(self)
        elif self.I == 0xF0:
            instr = RP(self)
        elif self.I == 0xE8:
            instr = RPE(self)
        elif self.I == 0xE0:
            instr = RPO(self)
        elif self.I == 0x0F:
            instr = RRC(self)
        elif self.I in [0xC7,0xCF,0xD7,0xDF,0xE7,0xEF,0xF7,0xFF]:
            instr = RST(self)
        elif self.I == 0xC8:
            instr = RZ(self)
        elif self.I in [0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F]:
            instr = SBB(self)
        elif self.I == 0xDE:
            instr = SBI(self)
        elif self.I == 0x22:
            instr = SHLD(self)
        #elif self.I == 0x30:
        #	instr = SIM(self)
        elif self.I == 0xF9:
            instr = SPHL(self)
        elif self.I == 0x32:
            instr = STA(self)
        elif self.I in [0x02,0x12]:
            instr = STAX(self)
        elif self.I == 0x37:
            instr = STC(self)
        elif self.I in [0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97]:
            instr = SUB(self)
        elif self.I == 0xD6:
            instr = SUI(self)
        elif self.I == 0xEB:
            instr = XCHG(self)
        elif self.I in [0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF]:
            instr = XRA(self)
        elif self.I == 0xEE:
            instr = XRI(self)
        elif self.I == 0xE3:
            instr = XTHL(self)
        else:
            instr = NUL(self)
        instr.Ejecutar()
        self.CiclosEjecutados = self.CiclosEjecutados + 1


