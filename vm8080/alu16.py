class ALU16(object):
    def __init__(self,p):
        self.CPU = p
        
    def Suma(self, h, l, op2):
        # Realiza la suma de 2 operandos de 8 bits 
        # con un operando de 16 bits
        # y actualiza Carry
        Valor = ((h << 8) | l) + op2
        if Valor > 65535:
                self.CPU.PSW.C = True
        else:
                self.CPU.PSW.C = False
        h = (Valor & 0xFF00) >> 8
        l = Valor & 0x00FF
        return h, l

    def Incremento(self, h, l):
        # Realiza la suma de 2 operandos de 8 bits 
        # con un operando de 16 bits
        Valor = ((h << 8) | l) + 1
        h = (Valor & 0xFF00) >> 8
        l = Valor & 0x00FF
        return h, l

    def Incremento16(self, op1):
        # Realiza la suma de 1 operando de 16 bits
        # con otro operando de 16 bits
        Valor = op1 + 1
        Valor = Valor & 0xFFFF
        return Valor

    def Decremento(self, h, l):
        # Realiza la resta de 2 operandos de 8 bits 
        # con un operando de 16 bits
        Valor = ((h << 8) | l) - 1
        if Valor < 0:
            self.CPU.PSW.C = True
        else:
            self.CPU.PSW.C = False
        h = (Valor & 0xFF00) >> 8
        l = Valor & 0x00FF
        return h, l

    def Decremento16(self, op1):
        # Realiza la resta de 1 operando de 16 bits
        # con otro operando de 16 bits
        Valor = op1 - 1
        if Valor < 0:
            self.CPU.PSW.C = True
        else:
            self.CPU.PSW.C = False
        Valor = Valor & 0xFFFF
        return Valor

