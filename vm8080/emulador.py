from .memoria import Memoria
from .procesador import Procesador
from .puertos import Puertos
from .pantalla import Pantalla
from .memoria import Memoria
#from .procesador import CPU
from .enums import *
import pygame

class Emulador(object):
    def __init__(self):
        self.Mem = Memoria()
        self.Puertos = Puertos()
#        self.Puertos.p[0] = 0xff
#        self.Puertos.p[1] = 0xff
#        self.Puertos.p[2] = 0xff
#        self.Puertos.p[3] = 0xff
        self.CPU = Procesador(self.Mem, self.Puertos)
        self.Pantalla = Pantalla(self.Mem)
        self.IntAB = False
        #self.RegistrarObservadorReset( ResetVideojuego )
        self.RegistrarObservadorVideo( self.Pantalla )

    def Reset(self):
        Mem.Reset()
        CPU.Reset()
        Puertos.Reset()

    def CargarROM(self,Nombre):
        f = open(Nombre, 'r')
        i = 0
        b = f.read(1)
        while b:
            #Esta es la unica parte de la maquina virtual
            #donde se escribe directamente a la memoria
            self.Mem.Mem[i] = b
            i = i + 1
            b = f.read(1)
        f.close

    def GuardarROM_(self,Nombre):
        f = open(Nombre, 'w')
        for i in range(0, MEMMAX):
            f.write(Mem.Leer(i))
        f.close

    def RegistrarObservadorReset(self,p):
        ObservadorReset = p

    def RegistrarObservadorVideo(self,p):
        self.Mem.ObservadorVideo = p

    def Ejecutar(self):
        int = 0x08
        while not self.CPU.mHALT:
            event = pygame.event.poll()
            if event.type == pygame.QUIT:
                    self.CPU.mHALT = True
            estado = pygame.key.get_pressed()
            # Tecla Coin Inserted
            if estado[pygame.K_m]:
                    #print "COIN INSERTED"
                    self.CPU.Puertos.EscribirBits(0x01, COIN_INSERTED)
            # Tecla One Player
            if estado[pygame.K_1]:
                    #print "ONE PLAYER"
                    self.CPU.Puertos.EscribirBits(0x01, ONE_PLAYER)
            # Tecla 2 Players
            elif estado[pygame.K_2]:
                    #print "TWO PLAYERS"
                    self.CPU.Puertos.EscribirBits(0x01, TWO_PLAYERS)
            # Tecla Player 1 Fire
            elif estado[pygame.K_SPACE]:
                    #print "PLAYER 1 FIRE"
                    self.CPU.Puertos.EscribirBits(0x01, P1_FIRE)
            # Tecla Player 1 Left
            elif estado[pygame.K_LEFT]:
                    #print "PLAYER 1 LEFT"
                    self.CPU.Puertos.EscribirBits(0x01, P1_LEFT)
            # Tecla Player 1 Right
                    #print "PLAYER 1 RIGHT"
            elif estado[pygame.K_RIGHT]:
                    self.CPU.Puertos.EscribirBits(0x01, P1_RIGHT)
            for i in range(1,INSTRUCCIONES_POR_FRAME):
                self.CPU.CicloVonNeumann()
            if self.CPU.InterrupcionesActivas:
                    self.CPU.Interrupcion(int)
                    if int == 0x10:
                            int = 0x08
                    elif int == 0x08:
                            int = 0x10                    
            pygame.display.flip()
