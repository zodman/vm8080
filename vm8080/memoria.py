from .enums import MEMMAX, VIDEO_MAX , ROM_MAX, VIDEO_MIN

class Memoria(object):

    def __init__(self):
        self.Mem = bytearray (MEMMAX+1)
        self.ObservadorVideo = 0
    
    def Reset(self):
        for i in range(0, MEMMAX):
            self.Mem[i] = 0x00

    def Leer(self,Addr):
        return self.Mem[Addr]

    def Escribir(self,Addr, Valor):
        if (Addr > ROM_MAX) and (Addr <= VIDEO_MAX):
            self.Mem[Addr] = Valor
        else:
    		print 'Tratando de escribir a ROM o memoria inexistente en ', hex(Addr)

        # Si hay un observador de video
        if self.ObservadorVideo:
            # Si la direccion cae dentro del rango de video
            if (Addr >= VIDEO_MIN) and (Addr <= VIDEO_MAX):
                self.ObservadorVideo.Actualizar(Addr)

