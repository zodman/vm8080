# encoding=utf8 
# Ancestro abstracto de todas las instrucciones
class Instruccion(object):
    def __init__(self, c):
        self.CPU = c

# Representa a cualquier opcode no válido
class NUL(Instruccion):
    def Ejecutar(self):
        ##print self.CPU.PC,': Opcode desconocido: ', self.CPU.I
        self.CPU.PC = self.CPU.PC + 1

    
# Ancestro comun a todas las instrucciones JMP, JC, JNC, etc.
class Salto(Instruccion):
    def Ejecutar(self):
        self.CPU.PC = self.CPU.ObtenerDireccionInmediata()

class SaltoCondicional(Salto):
    def Ejecutar(self, Condicion):
        if Condicion:
            super(SaltoCondicional,self).Ejecutar()
        else:
            self.CPU.PC = self.CPU.PC + 3

class AccesoMemoria(Instruccion):
    def __init__(self, cpu):
        super(AccesoMemoria, self).__init__(cpu)
        self.Mem = self.CPU.Mem

class Transferencia(AccesoMemoria):
    def Ejecutar(self, Valor8):
        ddd = self.CPU.DecodificarDestino()
        if ddd == 0:
            self.CPU.B = Valor8
        elif ddd == 1:
            self.CPU.C = Valor8
        elif ddd == 2:
            self.CPU.D = Valor8
        elif ddd == 3:
            self.CPU.E = Valor8
        elif ddd == 4:
            self.CPU.H = Valor8
        elif ddd == 5:
            self.CPU.L = Valor8
        elif ddd == 6:
            Addr = self.CPU.ObtenerDireccionHL()
            self.Mem.Escribir(Addr, Valor8)
        elif ddd == 7:
            self.CPU.A = Valor8

class Transferencia16(AccesoMemoria):
    def Ejecutar(self, pp, qq):
        sss = self.CPU.DecodificarRegisterPair()
        if sss == 0:
            self.CPU.B, self.CPU.C = pp,qq
        elif sss == 1:
            self.CPU.D, self.CPU.E = pp,qq
        elif sss == 2:
            self.CPU.H, self.CPU.L = pp,qq
        elif sss == 3:
            self.CPU.SP=  (pp << 8) | qq


# Ancestro comun a las instrucciones PUSH, CALL
class Apilado(AccesoMemoria):
    def Ejecutar(self, v1, v2):
        self.CPU.SP = self.CPU.SP - 1
        self.Mem.Escribir(self.CPU.SP, v1)
        self.CPU.SP = self.CPU.SP - 1
        self.Mem.Escribir(self.CPU.SP, v2)

# Ancestro comun a las instrucciones POP, RET
class Desapilado(AccesoMemoria):
    def Ejecutar(self):
        qq = self.Mem.Leer(self.CPU.SP)
        self.CPU.SP = self.CPU.SP + 1
        pp = self.Mem.Leer(self.CPU.SP)
        self.CPU.SP = self.CPU.SP + 1
        return pp,qq

# Ancestro comun a todas las instrucciones CALL, CC, CNC, etc.
class Subrutina(Apilado):
    def Ejecutar(self):
        Addr = self.CPU.PC + 3
        h = (Addr & 0xFF00) >> 8
        l = (Addr & 0x00FF)
        # En primer lugar, apilamos la direccion de retorno
        super(Subrutina,self).Ejecutar(h, l)
        # Despues cargamos el PC con la direccion inmediata
        self.CPU.PC = self.CPU.ObtenerDireccionInmediata()

class SubrutinaCondicional(Subrutina):
    def Ejecutar(self, Condicion):
        if Condicion:
            super(SubrutinaCondicional, self).Ejecutar()
        else:
            self.CPU.PC = self.CPU.PC + 3

# Ancestro comun a todas las instrucciones RET, RC, RNC, etc.
class Retorno(Desapilado):
    def Ejecutar(self):
        pp, qq = super(Retorno,self).Ejecutar()
        self.CPU.PC = (pp << 8) | qq
        ##print hex(self.CPU.PC)

class RetornoCondicional(Retorno):
    def Ejecutar(self,Condicion):
        if Condicion:
            super(RetornoCondicional,self).Ejecutar()
        else:
            self.CPU.PC = self.CPU.PC + 1

class NOP(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': NOP'
        self.CPU.PC = self.CPU.PC + 1

class HLT(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': HLT'
        self.CPU.mHALT = True
        self.CPU.PC = self.CPU.PC + 1

class AritmeticoLogica(Instruccion):
    def __init__(self, CPU):
        # Una abreviatura para el manejo de la ALU
        super(AritmeticoLogica,self).__init__(CPU)
        self.ALU = CPU.ALU

class AritmeticoLogica16(Instruccion):
    def __init__(self, CPU):
        # Una abreviatura para el manejo de la ALU
        super(AritmeticoLogica16,self).__init__(CPU)
        self.ALU16 = CPU.ALU16

class Suma(AritmeticoLogica):
    def Ejecutar(self,v1, v2, c):
        return self.ALU.Suma(v1, v2, c,)
    
class ACI(Suma):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ACI A,',
        Valor8 = self.CPU.ObtenerInmediato()
        self.CPU.A = super(ACI,self).Ejecutar(self.CPU.A, Valor8, self.CPU.PSW.C)
        self.CPU.PC = self.CPU.PC + 2
    
class ADC(Suma):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ADC A',
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        self.CPU.A = super(ADC,self).Ejecutar(self.CPU.A, Valor8, self.CPU.PSW.C)
        self.CPU.PC = self.CPU.PC + 1

class ADD(Suma):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ADD A',
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        self.CPU.A = super(ADD,self).Ejecutar(self.CPU.A, Valor8, False)
        self.CPU.PC = self.CPU.PC + 1

class ADI(Suma):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ADI A,',
        Valor8 = self.CPU.ObtenerInmediato()
        self.CPU.A = super(ADI,self).Ejecutar(self.CPU.A, Valor8, False)
        self.CPU.PC = self.CPU.PC + 2

class ANA(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        NOTA: El 8085 pone Ac=1 en todas las instrucciones AND
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ANA,',
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        self.CPU.A = self.ALU.And(self.CPU.A, Valor8)
        self.CPU.PC = self.CPU.PC + 1

class ANI(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(0) Ac(X) Z(X) S(X) P(X)
        NOTA: El 8085 pone Ac=1 en todas las intrucciones AND
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ANI A,',
        Valor8 = self.CPU.ObtenerInmediato() 
        self.CPU.A = self.ALU.And(self.CPU.A, Valor8)
        self.CPU.PC = self.CPU.PC + 2

class DAA(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': DAA'
        self.CPU.A = self.ALU.BCD(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class MOV(Transferencia):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': MOV ',
        # Obtenemos el valor de 8 bits que debemos mover
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        # Lo transferimos a su destino mediante el 
        # metodo Ejecutar() del ancestro Transferencia
        super(MOV,self).Ejecutar(Valor8)
        self.CPU.PC = self.CPU.PC + 1

class MVI(Transferencia):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': MVI ',
        # Obtenemos el valor de 8 bits que debemos mover
        Valor8  = self.CPU.ObtenerInmediato()
        # Lo transferimos a su destino mediante el
        # metodo Ejecutar() del ancestro Transferencia
        super(MVI, self).Ejecutar(Valor8)
        self.CPU.PC = self.CPU.PC + 2

class OUT(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': OUT ',
        NumPort= self.CPU.DecodificarPort()
        self.CPU.Puertos.Escribir(NumPort,self.CPU.A)
        self.CPU.PC = self.CPU.PC + 2

class IN(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': IN ',
        NumPort = self.CPU.DecodificarPort()
        self.CPU.A = self.CPU.Puertos.Leer(NumPort)
        self.CPU.PC = self.CPU.PC + 2

class RST(Apilado):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RST 0x', hex(self.CPU.I & 0x38)
        DirRetorno = self.CPU.PC + 1
        h = (DirRetorno & 0xFF00) >> 8
        l = (DirRetorno & 0x00FF)
        super(RST,self).Ejecutar(h, l)
        DirInterrupcion = 0x0000 | (self.CPU.I & 0x38)
        self.CPU.PC = DirInterrupcion

class HW_RST(Apilado):
    def Ejecutar(self, irq):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': HW RST 0x', hex(irq)
        DirRetorno = self.CPU.PC
        h = (DirRetorno & 0xFF00) >> 8
        l = (DirRetorno & 0x00FF)
        super(HW_RST,self).Ejecutar(h, l)
        DirInterrupcion = 0x0000 | (irq & 0x38)
        self.CPU.PC = DirInterrupcion 

class JMP(Salto):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JMP ',
        super(JMP,self).Ejecutar()

class RAL(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac( ) Z( ) S( ) P( )
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RAL'
        self.CPU.A = self.ALU.RotacionIzquierdaAcarreo(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class RAR(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac( ) Z( ) S( ) P( )
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RAR'
        self.CPU.A = self.ALU.RotacionDerechaAcarreo(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class Resta(AritmeticoLogica):
    def Ejecutar(self, v1, v2, b):
        return self.ALU.Resta(v1, v2, b)

class SUB(Resta):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': SUB A,',
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        self.CPU.A = super(SUB,self).Ejecutar(self.CPU.A, Valor8, False)
        self.CPU.PC = self.CPU.PC + 1

class SBB(Resta):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': SBB A,',
        sss = self.CPU.ObtenerOperandoOrigen()
        Valor8 = sss
        self.CPU.A = super(SBB,self).Ejecutar(self.CPU.A, Valor8, self.CPU.PSW.C)
        self.CPU.PC = self.CPU.PC + 1

class SUI(Resta):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': SUI A,',
        Valor8 = self.CPU.ObtenerInmediato()
        self.CPU.A = super(SUI,self).Ejecutar(self.CPU.A, Valor8, False)
        self.CPU.PC = self.CPU.PC + 2

class SBI(Resta):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': SBI A',
        Valor8= self.CPU.ObtenerInmediato()
        self.CPU.A = super(SBI,self).Ejecutar(self.CPU.A, Valor8, self.CPU.PSW.C)
        self.CPU.PC = self.CPU.PC + 2

class CMA(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C( ) Ac( ) Z( ) S( ) P( )
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CMA'
        self.CPU.A = self.ALU.Inversion(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class CMC(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CMC'
        self.CPU.PSW.C = not self.CPU.PSW.C
        self.CPU.PC = self.CPU.PC + 1

class CMP(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CMP A,',
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        self.ALU.Resta(self.CPU.A, Valor8, False)
        self.CPU.PC = self.CPU.PC + 1

class CPI(Resta):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CPI A,',
        Valor8 = self.CPU.ObtenerInmediato()
        temp = super(CPI,self).Ejecutar(self.CPU.A, Valor8, False)
        self.CPU.PC = self.CPU.PC + 2

class DAD(AritmeticoLogica16):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac( ) Z( ) S( ) P( )
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': DAD HL,',
        Valor16 = self.CPU.ObtenerValorRegisterPair()
        self.CPU.H, self.CPU.L = self.ALU16.Suma(self.CPU.H, self.CPU.L, Valor16)
        self.CPU.PC = self.CPU.PC + 1

class DCR(AritmeticoLogica):
    # NOTA: Dado que usa self.CPU.Mem tambien debiera descender
    # de la clase AccesoMemoria (Herencia Multiple)
    def Ejecutar(self):
        """
        Banderas Afectadas: C( ) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': DCR ',
        ddd = self.CPU.DecodificarDestino()
        if ddd == 0:
            self.CPU.B = self.ALU.Decremento(self.CPU.B)
        elif ddd == 1:
            self.CPU.C = self.ALU.Decremento(self.CPU.C)
        elif ddd == 2:
            self.CPU.D = self.ALU.Decremento(self.CPU.D)
        elif ddd == 3:
            self.CPU.E = self.ALU.Decremento(self.CPU.E)
        elif ddd == 4:
            self.CPU.H = self.ALU.Decremento(self.CPU.H)
        elif ddd == 5:
            self.CPU.L = self.ALU.Decremento(self.CPU.L)
        elif ddd == 6:
            Addr = self.CPU.ObtenerDireccionHL()
            Valor = self.ALU.Decremento( self.CPU.Mem.Leer(Addr))
            self.CPU.Mem.Escribir(Addr, Valor)
        elif ddd == 7:
            self.CPU.A = self.ALU.Decremento(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class DCX(AritmeticoLogica16):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': DCX ',
        ddd = self.CPU.DecodificarRegisterPair()
        if ddd == 0:
            self.CPU.B, self.CPU.C = self.ALU16.Decremento(self.CPU.B, self.CPU.C)
        elif ddd == 1:
            self.CPU.D, self.CPU.E = self.ALU16.Decremento(self.CPU.D, self.CPU.E)
        elif ddd == 2:
            self.CPU.H, self.CPU.L = self.ALU16.Decremento(self.CPU.H, self.CPU.L)
        elif ddd == 3:
            self.CPU.SP = self.ALU16.Decremento16(self.CPU.SP)
        self.CPU.PC = self.CPU.PC + 1

class INX(AritmeticoLogica16):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': INX ',
        ddd = self.CPU.DecodificarRegisterPair()
        if ddd == 0:
            self.CPU.B, self.CPU.C = self.ALU16.Incremento(self.CPU.B, self.CPU.C)
        elif ddd == 1:
            self.CPU.D, self.CPU.E = self.ALU16.Incremento(self.CPU.D, self.CPU.E)
        elif ddd == 2:
            self.CPU.H, self.CPU.L = self.ALU16.Incremento(self.CPU.H, self.CPU.L)
        elif ddd == 3:
            self.CPU.SP = self.ALU16.Incremento16(self.CPU.SP)
        self.CPU.PC = self.CPU.PC + 1

class DI(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': DI'
        self.CPU.InterrupcionesActivas = False
        self.CPU.PC = self.CPU.PC + 1

class EI(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': EI'
        self.CPU.InterrupcionesActivas = True
        self.CPU.PC = self.CPU.PC + 1

class STAX(AccesoMemoria):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': STAX ',
        Addr = self.CPU.ObtenerDireccionRegisterPair()
        self.Mem.Escribir(Addr, self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class INR(AritmeticoLogica):
    # NOTA: Dado que usa self.CPU.Mem tambien debiera descender
    # de la clase AccesoMemoria (Herencia Multiple)
    def Ejecutar(self):
        """
        Banderas Afectadas: C( ) Ac(X) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': INR ',
        ddd = self.CPU.DecodificarDestino()
        if ddd == 0:
            self.CPU.B = self.ALU.Incremento(self.CPU.B)
        elif ddd == 1:
            self.CPU.C = self.ALU.Incremento(self.CPU.C)
        elif ddd == 2:
            self.CPU.D = self.ALU.Incremento(self.CPU.D)
        elif ddd == 3:
            self.CPU.E = self.ALU.Incremento(self.CPU.E)
        elif ddd == 4:
            self.CPU.H = self.ALU.Incremento(self.CPU.H)
        elif ddd == 5:
            self.CPU.L = self.ALU.Incremento(self.CPU.L)
        elif ddd == 6:
            Addr = self.CPU.ObtenerDireccionHL()
            Valor = self.ALU.Incremento( self.CPU.Mem.Leer(Addr))
            self.CPU.Mem.Escribir(Addr, Valor)
        elif ddd == 7:
            self.CPU.A = self.ALU.Incremento(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class JC(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JC ',
        super(JC,self).Ejecutar(self.CPU.PSW.C)

class JM(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JM ',
        super(JM, self).Ejecutar(self.CPU.PSW.S)

class JNZ(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JNZ ',
        super(JNZ,self).Ejecutar(not self.CPU.PSW.Z)

class LDAX(AccesoMemoria):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': LDAX ',
        Addr = self.CPU.ObtenerDireccionRegisterPair()
        self.CPU.A = self.Mem.Leer(Addr)
        self.CPU.PC = self.CPU.PC + 1

class JP(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JP ',
        super(JP,self).Ejecutar(not self.CPU.PSW.S)

class JPE(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JPE ',
        super(JPE,self).Ejecutar(self.CPU.PSW.P)

class JPO(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JPO ',
        super(JPO,self).Ejecutar(not self.CPU.PSW.P)

class JNC(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JNC ',
        super(JNC,self).Ejecutar(not self.CPU.PSW.C)

class JZ(SaltoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': JZ ',
        super(JZ,self).Ejecutar(self.CPU.PSW.Z)

class LDA(AccesoMemoria):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': LDA ',
        Addr = self.CPU.ObtenerDireccionInmediata()
        self.CPU.A = self.Mem.Leer(Addr)
        self.CPU.PC = self.CPU.PC + 3

class LHLD(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': LHLD ',
        Addr = self.CPU.ObtenerDireccionInmediata()
        self.CPU.L = self.CPU.Mem.Leer(Addr)
        self.CPU.H = self.CPU.Mem.Leer(Addr + 1)
        self.CPU.PC = self.CPU.PC + 3

class LXI(Transferencia16):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': LXI ',
        pp, qq = self.CPU.ObtenerDobleInmediato()
        super(LXI,self).Ejecutar(pp, qq)
        self.CPU.PC = self.CPU.PC + 3

class ORA(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(0) Ac(0) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': OR A,',
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        self.CPU.A = self.ALU.Or(self.CPU.A, Valor8)
        self.CPU.PC = self.CPU.PC + 1

class ORI(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(0) Ac(0) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ORI A,',
        Valor8 = self.CPU.ObtenerInmediato()
        self.CPU.A = self.ALU.Or(self.CPU.A, Valor8)
        self.CPU.PC = self.CPU.PC + 2

class PCHL(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': PCHL'
        Valor = (self.CPU.H << 8) | self.CPU.L
        self.CPU.PC = Valor
"""
 OBSERVACION MUY IMPORTANTE: El tope de la pila senala al
 ultimo elemento almacenado, y no a la primera celda libre.
 Por tanto es necesario decrementar primero el SP y luego guardar.
 Ademas, primero se guarda el registro alto y despues el bajo,
 por ejemplo en BC se guarda primero B y despues C.
 Obviamente para POP sucede lo contrario.
 La misma logica se aplica a CALL y RET. RST actua como CALL.
"""
class PUSH(Apilado):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': PUSH ',
        sss = self.CPU.DecodificarRegisterPairPila()
        if sss == 0:
            super(PUSH,self).Ejecutar(self.CPU.B,self.CPU.C)
        elif sss == 1:
            super(PUSH,self).Ejecutar(self.CPU.D,self.CPU.E)
        elif sss == 2:
            super(PUSH,self).Ejecutar(self.CPU.H,self.CPU.L)
        elif sss == 3:
            # En la variable Valor guardamos los bits de PSW
            Valor = 0x00
            if self.CPU.PSW.C:
                Valor = Valor | 0x01
            #NOTA: Aparentemente el bit 2 siempre vale 1 (XXXXXX1X)
            if self.CPU.PSW.P:
                Valor = Valor | 0x04
            #NOTA: Aparentemente el bit 4 siempre vale 0 (XXXX0XXX)
            if self.CPU.PSW.Ac:
                Valor = Valor | 0x10
            #NOTA: Aparentemente el bit 6 siempre vale 0 (XX0XXXXX)
            if self.CPU.PSW.Z:
                Valor = Valor | 0x40
            if self.CPU.PSW.S:
                Valor = Valor | 0x80
            super(PUSH,self).Ejecutar(self.CPU.A,Valor)
        self.CPU.PC = self.CPU.PC + 1

class POP(Desapilado):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': POP ',
        sss = self.CPU.DecodificarRegisterPairPila()
        if sss == 0:
            self.CPU.B, self.CPU.C = super(POP,self).Ejecutar()
        elif sss == 1:
            self.CPU.D, self.CPU.E = super(POP,self).Ejecutar()
        elif sss == 2:
            self.CPU.H, self.CPU.L = super(POP,self).Ejecutar()
        elif sss == 3:
            self.CPU.A, Valor = super(POP,self).Ejecutar()
            #A partir de los bits de Valor ensamblamos los de PSW
            if (Valor & 0x01) != 0:
                self.CPU.PSW.C = True
            else:
                self.CPU.PSW.C = False
            if (Valor & 0x04) != 0:
                self.CPU.PSW.P = True
            else:
                self.CPU.PSW.P = False
            if (Valor & 0x10) != 0:
                self.CPU.PSW.Ac = True
            else:
                self.CPU.PSW.Ac = False
            if (Valor & 0x40) != 0:
                self.CPU.PSW.Z = True
            else:
                self.CPU.PSW.Z = False
            if (Valor & 0x80) != 0:
                self.CPU.PSW.S = True
            else:
                self.CPU.PSW.S = False
        self.CPU.PC = self.CPU.PC + 1

class CALL(Subrutina):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CALL ',
        super(CALL,self).Ejecutar()

class RET(Retorno):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RET ',
        super(RET,self).Ejecutar()

class CC(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CC ',
        super(CC,self).Ejecutar(self.CPU.PSW.C)

class CPO(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CPO ',
        super(CPO,self).Ejecutar(self.CPU.PSW.P)

class CM(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CM ',
        super(CM,self).Ejecutar(self.CPU.PSW.S)

class CNC(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CNC ',
        super(CNC,self).Ejecutar(not self.CPU.PSW.C)

class CNZ(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CNZ ',
        super(CNZ,self).Ejecutar(not self.CPU.PSW.Z)

class CP(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CP ',
        super(CP,self).Ejecutar(not self.CPU.PSW.S)

class CPE(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CPE ',
        super(CPE,self).Ejecutar(self.CPU.PSW.P)

class CZ(SubrutinaCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': CZ ',
        super(CZ,self).Ejecutar(self.CPU.PSW.Z)

class RC(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RC'
        super(RC,self).Ejecutar(self.CPU.PSW.C)

class RM(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RM'
        super(RM,self).Ejecutar(self.CPU.PSW.S)

class RNC(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RNC'
        super(RNC,self).Ejecutar(not self.CPU.PSW.C)

class RNZ(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RNZ'
        super(RNZ,self).Ejecutar(not self.CPU.PSW.Z)
    

class RP(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RP'
        super(RP,self).Ejecutar(not self.CPU.PSW.S)

class RPE(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RPE'
        super(RPE,self).Ejecutar(self.CPU.PSW.P)

class RPO(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RPO'
        super(RPO,self).Ejecutar(not self.CPU.PSW.P)

class RZ(RetornoCondicional):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RZ'
        super(RZ,self).Ejecutar(self.CPU.PSW.Z)

class RRC(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac( ) Z( ) S( ) P( )
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RRC'
        self.CPU.A = self.ALU.RotacionDerecha(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class RLC(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(X) Ac( ) Z( ) S( ) P( )
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': RLC'
        self.CPU.A = self.ALU.RotacionIzquierda(self.CPU.A)
        self.CPU.PC = self.CPU.PC + 1

class SHLD(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': SHLD'
        Addr = self.CPU.ObtenerDireccionInmediata()
        self.CPU.Mem.Escribir(Addr, self.CPU.L)
        self.CPU.Mem.Escribir(Addr+1, self.CPU.H)
        self.CPU.PC = self.CPU.PC + 3

class SPHL(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': SPHL'
        Valor16 = self.CPU.H << 8
        Valor16 = Valor16 | self.CPU.L
        self.CPU.SP = Valor16
        self.CPU.PC = self.CPU.PC + 1

class STA(AccesoMemoria):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': ST A,',
        Addr = self.CPU.ObtenerDireccionInmediata()
        self.Mem.Escribir(Addr, self.CPU.A)
        self.CPU.PC = self.CPU.PC + 3

class STC(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': STC'
        self.CPU.PSW.C = True
        self.CPU.PC = self.CPU.PC + 1

class XCHG(Instruccion):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': XCHG DE,HL'
        ValorD = self.CPU.D
        ValorE = self.CPU.E
        self.CPU.D = self.CPU.H
        self.CPU.E = self.CPU.L
        self.CPU.H = ValorD
        self.CPU.L = ValorE
        self.CPU.PC = self.CPU.PC + 1

class XRA(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(0) Ac(0) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': XOR A,',
        Valor8 = self.CPU.ObtenerOperandoOrigen()
        self.CPU.A = self.ALU.Xor(self.CPU.A, Valor8)
        self.CPU.PC = self.CPU.PC + 1

class XRI(AritmeticoLogica):
    def Ejecutar(self):
        """
        Banderas Afectadas: C(0) Ac(0) Z(X) S(X) P(X)
        """
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': XRI A,'
        Valor8 = self.CPU.ObtenerInmediato()
        self.CPU.A = self.ALU.Xor(self.CPU.A, Valor8)
        self.CPU.PC = self.CPU.PC + 2

class XTHL(AccesoMemoria):
    def Ejecutar(self):
        self.CPU.Desplegar()
        ##print hex(self.CPU.PC), ': XTHL'
        ValorH = self.CPU.H
        ValorL = self.CPU.L
        self.CPU.H = self.Mem.Leer(self.CPU.SP + 1)
        self.CPU.L = self.Mem.Leer(self.CPU.SP)
        self.Mem.Escribir(self.CPU.SP + 1, ValorH)
        self.Mem.Escribir(self.CPU.SP    , ValorL)
        self.CPU.PC = self.CPU.PC + 1

