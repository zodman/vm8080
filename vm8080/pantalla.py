import pygame
from .enums import VIDEO_MIN


class Pantalla(object):
    def __init__(self,m):
        self.ANCHO_PANTALLA = 256
        self.ALTO_PANTALLA  = 224
        self.ANCHO_PANTALLA_BYTES = 256 // 8

        self.Mem = m
        pygame.init()
        self.screen = pygame.display.set_mode( (self.ALTO_PANTALLA, self.ANCHO_PANTALLA) )
        self.screen.fill((0x00,0x00,0xff))
        pygame.display.flip()
    

    def Actualizar(self,Addr):
        NEGRO  = 0x00, 0x00, 0x00
        BLANCO = 0xFF, 0xFF, 0xFF
        # Coordenada del primero de los 8 pixeles contenidos
        # en la celda de memoria actual
        py = (Addr - VIDEO_MIN) // self.ANCHO_PANTALLA_BYTES
        bx = (Addr - VIDEO_MIN) %  self.ANCHO_PANTALLA_BYTES

        # El byte de memoria que contiene los 8 pixeles a dibujar
        b = self.Mem.Leer(Addr)
        for i in [7,6,5,4,3,2,1,0]:
            # Coordenada de cada uno de los 8 pixeles contenidos
            # en la celda de memoria actual
            px = self.ANCHO_PANTALLA - 8*bx - i
            # Color (0=negro, 1=blanco) del pixel actual
            Color = (b >> i) & 0x01
            if Color == 0:
                self.screen.set_at((py, px), NEGRO )
            else:
                self.screen.set_at((py, px), BLANCO )

