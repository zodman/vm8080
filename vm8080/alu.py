# encoding=utf8 
class ALU(object):
    def __init__(self,p):
        self.CPU = p
        
    def Suma(self, op1, op2, acarreo):
        Valor = op1 + op2
        if acarreo:
            Valor = Valor + 1
        self.CPU.PSW.ActualizarC(Valor)
        self.CPU.PSW.ActualizarAc(op1, op2, Valor)
        self.CPU.PSW.ActualizarZ(Valor)
        self.CPU.PSW.ActualizarS(Valor)
        self.CPU.PSW.ActualizarP(Valor)
        return Valor & 0xFF

    def Incremento(self, op):
        Valor = op + 1
        Valor = Valor & 0xFF
        # El Incremento NO actualiza C
        self.CPU.PSW.ActualizarAc(op, 0x01, Valor)
        self.CPU.PSW.ActualizarZ(Valor)
        self.CPU.PSW.ActualizarS(Valor)
        self.CPU.PSW.ActualizarP(Valor)
        return Valor

    def Resta(self, op1, op2, acarreo):
        Valor = op1 - op2
        if acarreo:
            Valor = Valor - 0x01
        self.CPU.PSW.ActualizarC(Valor)
        self.CPU.PSW.ActualizarAc(op1, op2, Valor)
        self.CPU.PSW.ActualizarZ(Valor)
        self.CPU.PSW.ActualizarS(Valor)
        self.CPU.PSW.ActualizarP(Valor)
        return Valor & 0xFF

    def Decremento(self, op):
        Valor = op - 1
        Valor = Valor & 0xFF
        # El Decremento NO actualiza C
        self.CPU.PSW.ActualizarAc(op, 0x1, Valor)
        self.CPU.PSW.ActualizarZ(Valor)
        self.CPU.PSW.ActualizarS(Valor)
        self.CPU.PSW.ActualizarP(Valor)
        return Valor

    def And(self, op1, op2):
        """
        En el Intel 8085 las operación AND siempre pone Ac en 1
        """
        Valor = op1 & op2
        self.CPU.PSW.C  = False
        self.CPU.PSW.AC = False
        self.CPU.PSW.ActualizarZ(Valor)
        self.CPU.PSW.ActualizarS(Valor)
        self.CPU.PSW.ActualizarP(Valor)
        return Valor

    def Or(self, op1, op2):
        Valor = op1 | op2
        self.CPU.PSW.C  = False
        self.CPU.PSW.Ac = False
        self.CPU.PSW.ActualizarZ(Valor)
        self.CPU.PSW.ActualizarS(Valor)
        self.CPU.PSW.ActualizarP(Valor)
        return Valor

    def Xor(self, op1, op2):
        Valor = op1 ^ op2
        self.CPU.PSW.C  = False
        self.CPU.PSW.Ac = False
        self.CPU.PSW.ActualizarZ(Valor)
        self.CPU.PSW.ActualizarS(Valor)
        self.CPU.PSW.ActualizarP(Valor)
        return Valor

    def Inversion(self, op):
        Valor = ~op
        return Valor

    def BCD(self, op):
        l = op & 0x0F
        h = (op & 0xF0) >> 4
        if (l > 9) or self.CPU.PSW.Ac:
            l = (l + 6) & 0x0F
            h = h + 1
            self.CPU.PSW.Ac = True
        else:
            self.CPU.PSW.Ac = False
            op = l & 0x0F
        if (h > 9) or self.CPU.PSW.C:
            h = h + 6
            self.CPU.PSW.C = not self.CPU.PSW.C
        else:
            self.CPU.PSW.C = False
        op = ( (h << 4) | l ) & 0xFF
        self.CPU.PSW.ActualizarZ(op)
        self.CPU.PSW.ActualizarS(op)
        self.CPU.PSW.ActualizarP(op)
        return op

    def RotacionIzquierdaAcarreo(self, op):
        # Primero, ponemos a salvo el valor actual de PSW.C
        if self.CPU.PSW.C:
            Cin = 0x01
        else:
            Cin = 0x00
        # Despues, rotamos los bits 1 posicion a la izquierda
        op = op << 1
        # Comprobamos se activo el 9avo. bit y lo transferimos a PSW.C
        if op > 255:
            self.CPU.PSW.C = True
        else:
            self.CPU.PSW.C = False
#NOTA: ¿Serán estos?
#        self.CPU.PSW.S = False
#        self.CPU.PSW.Ac = False
        # Retornamos el resultado
        return (op | Cin) & 0xFF

    def RotacionDerechaAcarreo(self, op):
        # Primero, ponemos a salvo el valor actual de PSW.C
        if self.CPU.PSW.C:
            Cout = 0x80
        else:
            Cout = 0x00
        # Comprobamos si esta activo el bit 1 y lo transferimos a PSW.C
        if (op & 0x01) == 0x01:
            self.CPU.PSW.C = True
        else:
            self.CPU.PSW.C = False
#NOTA: ¿Serán estos?
#        self.CPU.PSW.S  = False
#        self.CPU.PSW.Ac = False
        # Desplazamos todos los bits 1 posicion a la derecha
        op = op >> 1
        # Retornamos el resultado
        return (op | Cout) & 0xFF

    def RotacionIzquierda(self, op):
        # Desplazamos todos los bits 1 posicion a la izquierda
        op = op << 1
        # Si hubo acarreo lo transferimos a PSW.C
        if op > 255:
            self.CPU.PSW.C = True
        else:
            self.CPU.PSW.C = False
        # Si hubo acarreo, tambien lo transferimos al bit 1
        if self.CPU.PSW.C:
            op = op | 0x01
        return op & 0xFF

    def RotacionDerecha(self, op):
        # Si el bit 1 esta activo, lo transferimos a PSW.C
        if (op & 0x01) == 1:
            self.CPU.PSW.C = True
        else:
            self.CPU.PSW.C = False
        # Desplazamos todos los bits 1 posicion a la derecha
        op = op >> 1
        # Si hubo acarreo previo, lo transferimos al bit 8avo.
        if self.CPU.PSW.C:
            op = op | 0x80
        return op & 0xFF

