# encoding=utf8 
class PSW(object):
    def __init__(self):
        self.Reset()

    def Reset(self):
        self.C  = False
        self.S  = False
        self.P  = False
        self.Ac = False
        self.Z  = False

    def ActualizarC(self, Valor):
        #Actualización de la Bandera de Acarreo
        self.C = (Valor & 0x100) == 0x100

    def ActualizarAc(self, op1, op2, Valor):
        #Actualización de Bandera de Acarreo Auxiliar
        self.Ac = (Valor ^ op1 ^ op2) & 0x10 == 0x10

    def ActualizarP(self, Valor):
        #Actualizacion de Bandera de Paridad
        nbp = 0
        for conta in range(0, 7):
            nbp = nbp + ((Valor >> conta) & 0x01)
        if (nbp % 2) == 1:
            self.P = True
        else:
            self.P = False

    def ActualizarS(self, Valor):
        #Actualizacion de Bandera de Signo
        if (Valor & 0x80) != 0:	# 0x80 = 10000000
            self.S = True
        else:
            self.S = False

    def ActualizarZ(self, Valor):
        #Actualizacion de Bandera de Cero: PSW.Z
        if Valor & 0xFF == 0:
            self.Z = True
        else:
            self.Z = False

