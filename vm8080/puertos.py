from .enums import MAXPUERTOS

class Puertos(object):

    def __init__(self):
        self.p = bytearray(MAXPUERTOS+1)
        self.port2o  = 0x00
        self.port4hi = 0x00
        self.port4lo = 0x00

    def Reset(self):
        for i in range(0, MAXPUERTOS):
            self.p[i] = 0x00
        
    def Leer(self, Addr):
    #NOTA:	Estas condiciones seguramente tendran que estar
    #	en procedimientos Observador de lectura de los Puertos
        Valor = self.p[Addr]
        if Addr == 0x01:
            Valor = self.p[Addr]
            self.p[Addr] = self.p[Addr] & 0xFE
    	elif Addr == 0x03:
    	    Valor = ((((self.port4hi << 8) | self.port4lo) << self.port2o) >> 8) & 0xFF
        else:
            Valor = self.p[Addr]
        return Valor

    def Escribir(self,Addr, Valor):
        if Addr == 0x02:
            self.port2o = Valor
        elif Addr == 0x04:
            self.port4lo = self.port4hi
            self.port4hi = Valor
        else:
            self.p[Addr] = Valor

    def EscribirBits(self,Addr, Valor):
        self.p[Addr] = self.p[Addr] | Valor

