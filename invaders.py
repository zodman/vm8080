from vm8080 import Emulador
#from vm8000.enums import NAVES_DIP_2, JUEGO_EASY

def ResetVideojuego():
    Valor = NAVES_DIP_2 | JUEGO_EASY
    EscribirPort(0x02, Valor)

#PROGRAMA PRINCIPAL
def main():
    emulador = Emulador()

    emulador.CargarROM('invaders.rom')
    #Llamamos al ciclo principal de la aplicacion grafica

    emulador.Ejecutar()
    
if __name__ == '__main__':
    main()
